# Todo spec

## Initial page
* Go to todo app page
* Page contains "Create your first todo! :)"

## Add new todo

* Go to todo app page
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Todo list contains "Go to Istanbul Coders event ~(^-^)~"

## Add new todo and delete it

* Go to todo app page
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Delete todo item "Go to Istanbul Coders event ~(^-^)~"
* Page contains "Create your first todo! :)"

## Add and delete todos in mixed order

* Go to todo app page
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Page contains "Go to Istanbul Coders event ~(^-^)~"
* Add todo item "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Page contains "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Delete todo item "Go to Istanbul Coders event ~(^-^)~"
* Todo list not contains "Go to Istanbul Coders event ~(^-^)~"
* Add todo item "Learn cool stuff! (⌐■_■)"
* Page contains "Learn cool stuff! (⌐■_■)"
* Delete todo item "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Todo list not contains "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Delete todo item "Learn cool stuff! (⌐■_■)"
* Page contains "Create your first todo! :)"


## Add new todo and complete

* Go to todo app page
* Add todo item "Prepare to Istanbul Coders event (Not probably..) ¯\\_( ͡° ͜ʖ ͡°)_/¯"
* Add todo item "Go to Istanbul Coders event ~(^-^)~"
* Add todo item "Eat some pizza! (づ｡◕‿‿◕｡)づ"
* Add todo item "Learn cool stuff! (⌐■_■)"
* Click on todo "Go to Istanbul Coders event ~(^-^)~"
* Todo "Go to Istanbul Coders event ~(^-^)~" should be completed
* Click on todo "Learn cool stuff! (⌐■_■)"
* Todo "Learn cool stuff! (⌐■_■)" should be completed
* Wait "1" seconds
* Delete todo item "Prepare to Istanbul Coders event (Not probably..) ¯\\_( ͡° ͜ʖ ͡°)_/¯"
