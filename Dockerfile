FROM node:8 
WORKDIR /usr/src/app 
RUN npm install serve -g
COPY dist dist
EXPOSE 5000
CMD ["serve", "dist"] 