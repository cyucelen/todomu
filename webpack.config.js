const webpack = require("webpack");
const path = require("path");

module.exports = {
    devtool: "inline-source-map",
    entry: [
        "webpack/hot/only-dev-server",
        "./src"
    ],
    output: {
        path: `${__dirname}/dist`,
        filename: "index.js"
    },
    resolve: {
        modulesDirectories: ["node_modules", "src"],
        extensions: [
            "",
            ".js"
        ]
    },
    devServer: {
        contentBase: `${__dirname}/dist`,
        port: 8080
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                loaders: ["react-hot", "babel?presets[]=react,presets[]=es2015"],
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        })
    ]
};
